package com.example.cipherdemo.service;

import com.example.cipherdemo.util.RSAUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class LoginService {
    @RequestMapping("/index")
    public ModelAndView getIndex(HttpSession session){
        Map<String, String> keyMap = RSAUtil.genKeyPair();
        session.setAttribute("KEY_MAP",keyMap);
        return new ModelAndView("index");
    }
}
