package com.example.cipherdemo.util;

import javax.crypto.Cipher;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class RSAUtil {
    public static final Logger LOGGER=Logger.getLogger("RSAUtil");

    public static void main(String[] args) {
        /*genKeyPair();
        String content="是的发的发的发送到发送到发送sssss到发斯蒂芬000";
        System.out.println("公钥为:"+keyMap.get("publicKey"));
        String enResult=RSAEncrypt(content,keyMap.get("publicKey"));
        System.out.println(enResult);
        System.out.println("私钥为:"+keyMap.get("privateKey"));
        String deResult=RSADecrypt(enResult,keyMap.get("privateKey"));
        System.out.println(deResult);*/

    }


    public static  Map<String,String> genKeyPair(){
        try {
            KeyPairGenerator genKeyPair=KeyPairGenerator.getInstance("RSA");
            genKeyPair.initialize(1024,new SecureRandom());
            KeyPair keyPair = genKeyPair.generateKeyPair();
            Map<String,String> keyMap=new HashMap<>();
            PrivateKey privateKey = keyPair.getPrivate();
            keyMap.put("privateKey",Base64.getEncoder().encodeToString(privateKey.getEncoded()));
            PublicKey publicKey = keyPair.getPublic();
            keyMap.put("publicKey",Base64.getEncoder().encodeToString(publicKey.getEncoded()));
            return keyMap;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 使用公钥加密
     * @param content
     * @param publicKey
     * @return
     */
    public static String RSAEncrypt(String content,String publicKey){
        try {
            Cipher cipher=Cipher.getInstance("RSA");
            byte[] pub_bytes= Base64.getDecoder().decode(publicKey);
            X509EncodedKeySpec keySpec=new X509EncodedKeySpec(pub_bytes);
            KeyFactory keyFactory=KeyFactory.getInstance("RSA");
            PublicKey publicKey1=keyFactory.generatePublic(keySpec);
            cipher.init(Cipher.ENCRYPT_MODE,publicKey1);
            byte[] bytes_result = cipher.doFinal(content.getBytes("utf-8"));
            return Base64.getEncoder().encodeToString(bytes_result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String RSADecrypt(String content,String privateKey){
        try {
            byte[] pri_bytes=Base64.getDecoder().decode(privateKey);
            PKCS8EncodedKeySpec keySpec=new PKCS8EncodedKeySpec(pri_bytes);
            KeyFactory keyFactory=KeyFactory.getInstance("RSA");
            PrivateKey privateKey1 = keyFactory.generatePrivate(keySpec);
            Cipher cipher=Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE,privateKey1);
            byte[] bytes = cipher.doFinal(Base64.getDecoder().decode(content));
            String result= new String(bytes,"utf-8");
            return  result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
