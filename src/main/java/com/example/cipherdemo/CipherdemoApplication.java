package com.example.cipherdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CipherdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CipherdemoApplication.class, args);
    }

}
